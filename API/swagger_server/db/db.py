import os
import uuid

import mysql.connector as mariadb
from swagger_server.models.person import Person  # noqa: E501

try:
    print("Connecting to: " + os.environ['MYSQL_HOST'])
    print("User: " + os.environ['MYSQL_USER'])
    print("Pass to: " + os.environ['MYSQL_PASSWORD'])
    print("DB Name to: " + os.environ['MYSQL_DATABASE'])
    titanic = mariadb.connect(
        host=os.environ['MYSQL_HOST'],
        user=os.environ['MYSQL_USER'],
        passwd=os.environ['MYSQL_PASSWORD'],
        database=os.environ['MYSQL_DATABASE']
    )
    cursor = titanic.cursor()
    

except mariadb.Error as err:
    print("Error connecting to DB")
    exit()    

def get_all_records():
    sql = ("SELECT Survived,Pclass,Name,Sex,Age,Siblings_Spouses_Aboard,Parents_Children_Aboard,Fare,uuid FROM passengers")
    results = execute(sql)
    return results

def insert_person(person):
    myuuid = str(uuid.uuid4())
    sql = ("INSERT INTO passengers "
    "(Survived,Pclass,Name,Sex,Age,Siblings_Spouses_Aboard,Parents_Children_Aboard,Fare,uuid) "
    "VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)")
    execute(sql, True, [person.survived, person.passenger_class, person.name, person.sex, person.age, person.siblings_or_spouses_aboard, person.parents_or_children_aboard, person.fare, myuuid ], True)
    sql = ("SELECT Survived,Pclass,Name,Sex,Age,Siblings_Spouses_Aboard,Parents_Children_Aboard,Fare,uuid FROM passengers WHERE uuid = %(uuid)s")
    result = get_person(myuuid)
    return result

def update_person(uuid, person):
    sql = ("UPDATE passengers "
    "SET Survived = %s,"
    "Pclass = %s,"
    "Name = %s,"
    "Sex = %s,"
    "Age = %s,"
    "Siblings_Spouses_Aboard = %s,"
    "Parents_Children_Aboard = %s,"
    "Fare = %s,"
    "uuid = %s "
    "WHERE uuid = %s")
    execute(sql, True, [person.survived, person.passenger_class, person.name, person.sex, person.age, person.siblings_or_spouses_aboard, person.parents_or_children_aboard, person.fare, uuid, uuid ], True)
    
def delete_person(uuid):
    sql = ("DELETE FROM passengers WHERE uuid = %(uuid)s")
    result = execute(sql, True, {'uuid':uuid}, True)
    return result
   

def get_person(uuid):
    sql = ("SELECT Survived,Pclass,Name,Sex,Age,Siblings_Spouses_Aboard,Parents_Children_Aboard,Fare,uuid FROM passengers WHERE uuid = %(uuid)s")
    result = execute(sql, True, {'uuid':uuid})
    return result

def execute(tuple, single = False, args = {}, commit = False):
    cursor.execute(tuple, args)

    if commit == True:
        titanic.commit()
    else:
        if single == True:
            return cursor.fetchone()
        else:
            return cursor.fetchall()