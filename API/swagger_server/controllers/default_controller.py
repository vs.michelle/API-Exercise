import connexion
import six
import json

from swagger_server.db.db import get_all_records
from swagger_server.db.db import insert_person
from swagger_server.db.db import get_person
from swagger_server.db.db import delete_person
from swagger_server.db.db import update_person
from swagger_server.models.people import People  # noqa: E501
from swagger_server.models.person import Person  # noqa: E501
from swagger_server.models.person_data import PersonData  # noqa: E501
from swagger_server import util




def people_add(person):  # noqa: E501
    """Add a person to the database

     # noqa: E501

    :param person: 
    :type person: dict | bytes

    :rtype: Person
    """
    if connexion.request.is_json:
        person = PersonData.from_dict(connexion.request.get_json())  # noqa: E501
        result = insert_person(person)
    return Person(result[0],result[1],result[2],result[3],result[4],result[5],result[6],result[7],result[8])


def people_list():  # noqa: E501
    """Get a list of all people

     # noqa: E501


    :rtype: People
    """
    results = get_all_records()
    people = []
    for row in results:
        pip = Person(row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8])
        people.append(pip)
    return people


def person_delete(uuid):  # noqa: E501
    """Delete this person

     # noqa: E501

    :param uuid: 
    :type uuid: dict | bytes

    :rtype: None
    """
    delete_person(uuid)
    return 


def person_get(uuid):  # noqa: E501
    """Get information about one person

     # noqa: E501

    :param uuid: 
    :type uuid: dict | bytes

    :rtype: Person
    """
    result = get_person(uuid)
    pip = Person(result[0],result[1],result[2],result[3],result[4],result[5],result[6],result[7],result[8])
    return pip


def person_update(uuid, person):  # noqa: E501
    """Update information about one person

     # noqa: E501

    :param uuid: 
    :type uuid: dict | bytes
    :param person: 
    :type person: dict | bytes

    :rtype: Person
    """
    if connexion.request.is_json:
        person = PersonData.from_dict(connexion.request.get_json())  # noqa: E501
        update_person(uuid, person)
    return
