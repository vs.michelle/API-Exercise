##Titanic API

This  project was to create a database and API that runs on Kubernetes.
I am testing this locally on minikube

##Getting Started

Please clone the following project from Gitlab: https://gitlab.com/vs.michelle/API-Exercise.git

##Prerequisites

Docker
MiniKube

##Deploying DB & API

Please run the deploy.sh script in the root folder to deploy the database container & API container on minikube in the titanic-api namespace.

It will take a minute to start-up the db pod and you might see the api pod restart a few times until the db is ready.

minikube will open the browser.

This can be accesss here minikubeip:port/ui

UI URL: 
http://192.168.99.100:30778/ui

Request URL: http://192.168.99.100:30778/people

##Example API calls:

**List all people:**
curl -X GET --header 'Accept: application/json' 'http://192.168.99.100:30778/people'

**Insert person:**
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ \ 
   "age": 6, \ 
   "fare": 5.637376656633329, \ 
   "name": "John", \ 
   "parentsOrChildrenAboard": 5, \ 
   "passengerClass": 0, \ 
   "sex": "male", \ 
   "siblingsOrSpousesAboard": 1, \ 
   "survived": true \ 
 }' 'http://192.168.99.100:30778/people'

**For more usage information please refer to the ui**

##Authors

Michelle Pretorius

##Acknowledgments

Used Swagger code generation api