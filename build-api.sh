#!/bin/bash

cd API/
docker build -t registry.gitlab.com/vs.michelle/api-exercise/titanic-api:latest .
cd ..

#Run the below command to push to docker registry
#docker push registry.gitlab.com/vs.michelle/api-exercise/titanic-api:latest