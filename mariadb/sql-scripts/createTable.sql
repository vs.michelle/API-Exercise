CREATE TABLE IF NOT EXISTS passengers (
    `uuid` char(36) NULL,	
    `Survived` INT,
    `Pclass` INT,
    `Name` VARCHAR(81) CHARACTER SET utf8,
    `Sex` VARCHAR(6) CHARACTER SET utf8,
    `Age` NUMERIC(4, 2),
    `Siblings_Spouses_Aboard` INT,
    `Parents_Children_Aboard` INT,
    `Fare` NUMERIC(7, 4)
);

DELIMITER ;;
CREATE TRIGGER before_insert_passengers
BEFORE INSERT ON passengers
FOR EACH ROW
BEGIN
  IF new.uuid IS NULL THEN
    SET new.uuid = uuid();
  END IF;
END
;;