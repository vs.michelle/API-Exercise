#!/bin/bash

cd K8s/
kubectl delete -f deploy.yaml
kubectl create -f deploy.yaml
minikube service -n titanic-api titanic-api