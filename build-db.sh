#!/bin/bash

cd mariadb/
docker build -t registry.gitlab.com/vs.michelle/api-exercise/titanic-mariadb:latest .
cd ..

#Run the below command to push to docker registry
#docker push registry.gitlab.com/vs.michelle/api-exercise/titanic-mariadb:latest